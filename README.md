# Filipův hokej
Projekt dynamické webové stránky

## Úvod
Filipův hokej je školní projekt, který se zaměřuje na vytvoření dynamické webové stránky s využitím PHP a MySQL. Kromě toho se na stránce vyskytuje minimum javascriptu, který se stará o menší, estetické úkony na popředí.

## Využité technologie
- PHP
- MySQL
- Javascript
- SCSS
- CSS
