var seznam = document.getElementById("menuLinks");
var ikona = document.getElementById("menuBtn");
function menuToggle() {
    if (seznam.style.display === "none") {
        seznam.style.display = "block";
        ikona.innerHTML = "&#x2716;";
    } else {
        seznam.style.display = "none";
        ikona.innerHTML = "&#9776;";
    }

    if (document.body.clientWidth >= 1024) {
        seznam.style.display = "inline-block";
    }
}