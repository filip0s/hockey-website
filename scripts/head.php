<?php
session_start();
include_once('./scripts/connection.php');
if(isset($_GET[$page])) {
    $page = $_GET["$page"];
} else {
    $page = "home";
}


?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <!--meta informace-->
    <meta name="author" content="Filip">
    <meta name="description" content="Stránky o ledním hokeji">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> | Filipův hokej</title>
    <!--import-->
    <script src="https://kit.fontawesome.com/ba79d9845b.js"></script>
    <link rel="stylesheet" href="css/mobile.css" type="text/css" media="screen and (max-width:1023px)">
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen and (min-width:1024px)">
</head>
