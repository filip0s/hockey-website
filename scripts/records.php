<?php
	function getRecord(){
		$records = array(	'Gordie Howe odehrál nejvíce zápasů v NHL, 1767',
							'Nejvíce zápasů v řadě v NHL odehrál Doug Jarvis, 964',
							'Gordie Howe a Chris Chelios odehráli 26 sezón v NHL',
							'Wayne Gretzky nastřílel v NHL 894 gólů',
							'Wayne Gretzky si za svou kariéru v NHL připsal 1963 asistencí',
							'Wayne Gretzky nasbíral během 1487 zápasů v NHL 2857 bodů. Druhý v pořadí, Jaromír Jágr na něj ztrácí 936 bodů',
							'Potřebujete někoho na nájezdy, tak Frans Nielsen je vaše volba. V nájezdech nastřílel 49 gólů a má úspěšnost 47,6 %',
							'Wayne Gretzky v průměru na jeden zápas nasbíral 1,92 bodu',
							'Wayne Gretzky si v 50 zápasech připsal v zápase 3 a více gólů',
							'Jaromír Jágr vstřelil 135 vítězných branek',
							'Bep Guidolin je nejmladším hráčem v historii, který vstřelil gól v NHL. Bylo mu 16 let a 350 dní, když vstřelil svou první branku',
							'Montreal Canadiens získali 23× Stanley Cup',
							'Největší počet diváků navštívil hokejový zápas mezi Michigan Wolverines a Michigan State Spartans. Tehdy bylo ohlášeno 113 411 diváků',
							'Nejvíce zápasů v Extralize odehrál František Ptáček, 1033',
							'Nejvíce gólů (335) v Extralize nastřílel Petr Ton',
							'Nejvíce gólů během jedné sezóny (40) v Extralize nastřílel Martin Růžička',
							'Nejvíce utkání v Extralize odchytal Jiří Trvaj, 640',
							'Nejvíce výher v Extraligové kariéře jako brankář má Adam Svoboda, 263 zápasů');
		$num = mt_rand(0,count($records)-1);
		$output = $records[$num];
		return $output;
	}

?>