<?php
//deklarace proměnných
$db_database = "hockeydb";
$db_userName = "root";
$db_password = "";
$db_server = "localhost";

//připojení
$con = mysqli_connect($db_server,$db_userName,$db_password,$db_database);
mysqli_query($con, "SET NAMES utf8");

//kontrola připojení
if(!$con)
    {
        die('<script>alert("Connection failed ' . mysqli_connect_error() . '")</script>;');
    }
?>