<?php
function kolo(
    $playerName,
    $playerStrength,
    $playerEndurance,
    $enemyName,
    $enemyStrength,
    $enemyEndurance)
    {
    $enemyDamage = 0;
    $playerDamage = 0;
    $firstMove = mt_rand(1,2);
    if ($firstMove == 1){
        $secondMove = mt_rand(1,2);
        if ($secondMove == 1){
            if ($playerStrength > 7) {
                $enemyDamage = mt_rand(25,50);
            } else {
                $enemyDamage = mt_rand(5,15);
            }
            $outputMessage = "$playerName úspěšně zaútočil a sebral " . $enemyName . "ovi $enemyDamage životů";
            
        } else {
            $outputMessage = "$enemyName vykryl tuto ránu";
        }
    } else {
        $secondMove = mt_rand(1,2);
        if($secondMove == 1) {
            $outputMessage = "$playerName vykryl tuto ránu";
        } else {
            if ($enemyStrength > 7) {
                $playerDamage = mt_rand(25,50);
            } else {
                $playerDamage = mt_rand(5,15);
            }
            $outputMessage = "$enemyName úspěšně zaútočil a sebral " . $playerName . "ovi $playerDamage životů";
        }
    }
    $output = array($outputMessage,$playerDamage,$enemyDamage);
    return $output;
}