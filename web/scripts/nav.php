<nav>
    <a class="logo" href=""><b>Filipův</b>Hokej</a>
    <ul id="menuLinks">
        <li><a href="index.php?page=home">Domů</a></li>
        <li><a href="index.php?page=players">Hráči</a></li>
        <li><a href="index.php?page=teams">Týmy</a></li>
        <li><a href="index.php?page=leagues">Ligy</a></li>
        <li><a href="index.php?page=rules">Pravidla</a></li>
        <li><a href="index.php?page=minigame">Minihra</a></li>
    </ul>
    <a href="javascript:void(0)" class="menuBtn" onclick="menuToggle()"><i class="fas fa-bars"></i></a>
</nav>
<script type="text/javascript" src="scripts/main.js"></script>
<?php
    switch ($page):
        case 'home':
            include "home.php";
            break;
        
        case 'players':
            include "players.php";
            break;

        case 'teams':
            include "teams.php";
            break;
        
        case 'rules':
            include "rules.php";
            break;

        case 'minigame':
            include "minigame.php";
            break;

        default: 
            include "home.php";
            break;
    endswitch;
?>