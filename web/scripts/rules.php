<?php
    function getRule(){
        $rules = array(
            'Hřiště pro lední hokej má rozměry 61×30m',
            'Během hry má na ledě jeden tým 5 hráčů v poli + 1 brankáře',
            'Každý tým může do zápasu nasadit maximálně 20 hráčů',
            'Zápas řídí celkem 4 rozhodčí - 2 hlavní a 2 čároví',
            'Hokejový zápas se hraje na 3 třetiny, které trvají 20 minut. Mezi třetinami je přestávka 15 minut na úpravu ledové plochy',
            'Pokud se hokejový zápas nerozhodne po 60 minutách hry, hraje se prodloužení, které se hraje stylem náhlá smrt',
            'Pokud hokejový zápas nerozhodne prodloužení, o vítězi se rozhodne v nájezdech, kde má každý tým 5 pokusů na vstřelení branky',
            'Lední hokej se hraje gumovým pukem, který má průměr 7,62 cm, tloušťku 2,54 cm a hmotnost 156-170g',
            'Lední hokej je jediný nebojový sport, ve kterém se hráči můžou porvat a stále pokračovat v zápase.',
            'Prodloužení se může 5, 10 nebo 20 minut. Záleží na ligách, jestli se hraje prodloužení pouze jedno, nebo jestli se prodlužuje do doby, než padne gól'
        );

    return $rules[array_rand($rules)];
    }
?>