<main>
    <div class="responsiveTable">
        <table class="players">
            <thead>
                <tr>
                    <th class="mainInfo">Fotka</th>
                    <th class="mainInfo">Jméno</th>
                    <th class="mainInfo">Tým</th>
                    <th>Číslo</th>
                    <th>Hůl</th>
                    <th>Post</th>
                    <th>Datum naroznení</th>
                </tr>
            </thead>
            <tbody>
        <?php
            $sql = "SELECT players.player_id, players.player_profile_link, players.first_name, players.last_name, players.avatar_photo, players.date_of_birth, players.player_number, players.post_id, players.hands_id, teams.team_logo, teams.team_id, hands.hands_id, hands.hands_name, posts.post_name FROM ( ( ( players INNER JOIN teams ON players.team_id = teams.team_id ) INNER JOIN hands ON players.hands_id = hands.hands_id ) INNER JOIN posts ON players.post_id = posts.post_id ) ORDER BY players.last_name ";
            $result = mysqli_query($con,$sql);
            while ($row = mysqli_fetch_assoc($result))
            {
                echo "\t\t<tr><td><img class='avatar mainInfo' src='img/avatars/{$row['avatar_photo']}' alt='{$row['avatar_photo']}'></td><td class='mainInfo'><a href='{$row['player_profile_link']}' onclick='leavingPage()' target='_blank'>{$row['first_name']} {$row['last_name']}</a></td><td><img class='avatar mainInfo' src='img/logos/{$row['team_logo']}' alt='{$row['team_logo']}'></td><td>#{$row['player_number']}</td><td>{$row['hands_name']}</td><td>{$row['post_name']}</td><td>{$row['date_of_birth']}</td></tr>\n";
            }
        ?>
        <tbody>
        </table>
    </div>
</main>
<script src="scripts/leavingPage.js"></script>