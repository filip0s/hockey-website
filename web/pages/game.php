<?php
	require_once '../scripts/connection.php';
	$playerName = html_entity_decode($_GET['playerName']);
	$playerStrength = mt_rand(1,10);
	$playerEndurance = mt_rand(1,10);
	$playerPoints = 0;
	if ($playerEndurance == 10) {$playerHealth = 200;} else {$playerHealth = 100;}
	$sql = "INSERT INTO `characters`(`character_id`, `character_name`, `character_strength`, `character_endurance`, `character_points`) VALUES (null,'$playerName','$playerStrength','$playerEndurance',0)";

	function newEnemy(){
		$names = array('Tom','Pepa','Petr','Antonín','Jan','Mirek','Andrej','Ivan','Alex','Václav');
		$enemyName = $names[array_rand($names)];
		$enemyStrength = mt_rand(1,10);
		$enemyEndurance = mt_rand(1,10);
		if($enemyEndurance == 10){$enemyHealth = 200;} else {$enemyHealth = 100;}
		$output = array($enemyName,$enemyStrength,$enemyEndurance,$enemyHealth);
		return $output;
	}

	$enemyAttributes = newEnemy();
	$enemyName = $enemyAttributes[0];
	$enemyStrength = $enemyAttributes[1];
	$enemyEndurance = $enemyAttributes[2];
	$enemyHealth = $enemyAttributes[3];

	include_once '../scripts/game_scripts.php';	
?>
<!DOCTYPE html>
<html lang="cs">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="../css/game.css">
	<link rel="icon" href="../ico/icon_32.png" type="image/x-icon">
	<title>Minihra | Filipův hokej</title>
</head>
<body>
	<main>
		<a href="../index.php?page=minigame"><div class="btn">&#x2B05; Zpět na stránky</div></a>
		<?php

		?>
			<div id="outputBox">
				<?php
					do{	
						$vysledky =  kolo($playerName,$playerStrength,$playerEndurance,$enemyName,$enemyStrength,$enemyEndurance);
						$playerDamage = $vysledky[1];
						$enemyDamage = $vysledky[2];
						$zprava = $vysledky[0];
						if($playerDamage > 0) {$playerHealth-=$playerDamage;}
						if($enemyDamage > 0) {$enemyHealth-= $enemyDamage;}
						echo "<p>$zprava</p>";
						echo "<p>$playerName životy: $playerHealth</p>";
						echo "<p>$enemyName životy: $enemyHealth</p>";
						echo "<br>";
						echo "<br>";
						$playerPoints += $playerDamage;
					} while (($playerHealth > 0) and ($enemyHealth > 0));
				if ($playerHealth > $enemyHealth) {
					echo "<span class='victory'>$playerName vyhrál</span>";
					echo "<p>Jedem dál!</p>";
				} else {
					echo "<span class='vicory'>$enemyName vyhrál</span>";
					echo "<p class='victory'>Z této bitky jsi získal $playerPoints bodů</p>";
				}
				;?>
			</div>
			<div id="character" class="profile">
				<img class="avatar" src="../img/avatars/game_avatars/avatar.png" alt="avatar">
				<h2><?php echo $playerName;?></h2>
				<span>Zdraví:</span><br>
				<progress value="<?php echo $playerHealth?>" max="100"></progress><br>
				<span>Síla: <b><?php echo $playerStrength?></b></span><br>
				<span>Vytrvalost: <b><?php echo $playerEndurance?></b></span>
			</div>
			<div id="enemy" class="profile">
				<img class="avatar" src="../img/avatars/game_avatars/avatar.png" alt="avatar">
				<h2><?php echo $enemyName;?></h2>
				<span>Zdraví:</span><br>
				<progress value="<?php echo $enemyHealth;?>" max="100"></progress><br>
				<span>Síla: <b><?php echo $enemyStrength;?></b></span><br>
				<span>Vytrvalost: <b><?php echo $enemyEndurance;?></b></span>
			</div>
			
	</main>
	<script>
	</script>
</body>
</html>

