<main>
    <div class="profileCardGrid">
        <?php
            $sql = "SELECT teams.team_id,teams.team_profile_link, teams.number_of_championships,teams.year_of_establishment, teams.city_id, teams.country_id, teams.team_name, teams.team_logo, countries.country_id, countries.country_name, cities.city_id, cities.city_name FROM ( ( teams INNER JOIN countries ON teams.country_id = countries.country_id ) INNER JOIN cities ON teams.city_id = cities.city_id ) ORDER BY teams.team_name";
            $query = mysqli_query($con,$sql);
            while ($row = mysqli_fetch_assoc($query)) {
                echo "
                    <div class='card'>
                        <img class='avatarBig' alt='{$row['team_logo']}' src='img/logos/{$row['team_logo']}'>
                        <a href='{$row['team_profile_link']}' onclick='leavingPage()' target='_blank'><h2>{$row['team_name']}</h2></a>
                        <span>&#x1F4CD; {$row['city_name']}, {$row['country_name']} | &#128197; {$row['year_of_establishment']} | {$row['number_of_championships']} × &#x1f3c6;</span>
                    </div>
                ";
            }
        ?>
    </div>
</main>
<script src="scripts/leavingPage.js"></script>