<main>
    <div class="profileCardGrid">
        <?php
            $sql = "SELECT leagues.league_name,leagues.league_profile_link, leagues.league_logo,leagues.year_of_establishment, leagues.main_prize, leagues.league_id,leagues.country_id, countries.country_id, countries.country_name FROM ( leagues INNER JOIN countries ON leagues.country_id = countries.country_id) ORDER BY leagues.year_of_establishment;";
            $query = mysqli_query($con,$sql);
            while ($row = mysqli_fetch_assoc($query)){
                echo "
                    <div class='card'>
                        <img class='avatarBig' src='img/logos/{$row['league_logo']}' alt='{$row['league_logo']}'>
                        <a href='{$row['league_profile_link']}' onclick='leavingPage()' target='_blank'><h2>{$row['league_name']}</h2></a>
                        <span>&#x1F4CD {$row['country_name']} | &#x1f3c6 {$row['main_prize']} </span>
                    </div>
                ";
            }
        ?>
    </div>
</main>
<script src="scripts/leavingPage.js"></script>