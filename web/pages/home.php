<main class="grid-1">
    <article >
        <div class="container">
            <h3>Hráči</h3> 
            <p>Poznejte nejlepší hráče, kteří brázdí kluziště v nejlepších ligách, ale i u nás. Od ostrostřelců s přesnou muškou, až po brankáře, kteří jsou schopní svůj tým podržet v jakýchkoli situacích.</p>
            <div class="centerContainer"><div class="btn"><a href="index.php?page=players">Přehled hráčů</a></div></div>
        </div>
    </article>
    <article class="widget">
        <div class="container">   
            <h3>Náhodný rekord</h3>
            <?php include 'scripts/records.php';
            echo "<p>" . getRecord() . "</p>";
            ?>
            <!--TODO:-->
        </div>
    </article>
    <article>
        <div class="container">
            <h3>Ligy</h3>
            <p>Poznejte špičkové ligy, kde proti sobě soutěží nejlepší týmy plné výbroných hráčů.</p>
            <div class="centerContainer"><div class="btn"><a href="index.php?page=leagues">Přehled lig</a></div></div>
        </div>
    </article>
    <article>
        <div class="container">
            <?php include 'scripts/countdown.php'; echo getTimer();?>
            <h3>Odpočet</h3>
            <p>Chcete vidět hokej na vlastní oči? Stačí počkat 
            <span id="odpocet"></span> a můžete se jít podívat na některý z extraligových stadionů</p>
            <!--TODO:-->
        </div>
    </article>
    <article>
        <div class="container">
            <h3>Týmy</h3>
            <p><i>"There is no I in a word team".</i><br>I nejlepší hráč je v hokeji nikdo, pokud nemá spoluhráče, kteří mu pomáhají.</p>
            <div class="centerContainer"><div class="btn"><a href="index.php?page=teams">Přehled týmů</a></div></div>
        </div>
    </article>
    <article class="widget">
        <div class="container">
            <h3>Víte, že</h3>
            <?php include 'scripts/rules.php'; echo "<p>".getRule()."</p>";?>
        </div>
    </article>
</main>
