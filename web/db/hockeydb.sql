-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pon 10. čen 2019, 03:53
-- Verze serveru: 10.1.40-MariaDB
-- Verze PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `hockeydb`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `characters`
--

CREATE TABLE `characters` (
  `character_id` int(11) NOT NULL,
  `character_name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `character_strength` int(11) NOT NULL,
  `character_endurance` int(11) NOT NULL,
  `character_points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `cities`
--

CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(64) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `cities`
--

INSERT INTO `cities` (`city_id`, `city_name`) VALUES
(1, 'Brno'),
(2, 'Třebíč'),
(3, 'Montréal'),
(4, 'Kelč'),
(5, 'Vancouver'),
(6, 'Québec');

-- --------------------------------------------------------

--
-- Struktura tabulky `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(64) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`) VALUES
(1, 'Česká Republika'),
(2, 'USA'),
(3, 'Kanada'),
(4, 'Rusko'),
(5, 'Kanada, USA'),
(6, 'Evropa, Asie');

-- --------------------------------------------------------

--
-- Struktura tabulky `hands`
--

CREATE TABLE `hands` (
  `hands_id` int(11) NOT NULL,
  `hands_name` varchar(16) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `hands`
--

INSERT INTO `hands` (`hands_id`, `hands_name`) VALUES
(1, 'levá'),
(2, 'pravá');

-- --------------------------------------------------------

--
-- Struktura tabulky `leagues`
--

CREATE TABLE `leagues` (
  `league_id` int(11) NOT NULL,
  `league_name` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `main_prize` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `bio` mediumtext COLLATE utf8_czech_ci NOT NULL,
  `league_logo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `header_photo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `year_of_establishment` year(4) NOT NULL,
  `country_id` int(11) NOT NULL,
  `league_profile_link` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `leagues`
--

INSERT INTO `leagues` (`league_id`, `league_name`, `main_prize`, `bio`, `league_logo`, `header_photo`, `year_of_establishment`, `country_id`, `league_profile_link`) VALUES
(1, 'NHL', 'Stanley Cup', '', 'nhl.png', 'nhl.png', 1917, 5, 'https://www.eliteprospects.com/league/nhl'),
(2, 'Extraliga', 'Masarykův pohár', '', 'extraliga.png', 'extraliga.png', 1993, 1, 'https://www.eliteprospects.com/league/czech'),
(3, 'KHL', 'Gagarinův pohár', '', 'khl.png', 'khl.png', 2008, 6, 'https://www.eliteprospects.com/league/khl');

-- --------------------------------------------------------

--
-- Struktura tabulky `players`
--

CREATE TABLE `players` (
  `player_id` int(11) NOT NULL,
  `first_name` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `player_number` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `hands_id` int(11) NOT NULL,
  `date_of_birth` date NOT NULL,
  `city_of_birth_id` int(11) NOT NULL,
  `country_of_birth_id` int(11) NOT NULL,
  `bio` mediumtext COLLATE utf8_czech_ci NOT NULL,
  `avatar_photo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `header_photo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `player_profile_link` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `players`
--

INSERT INTO `players` (`player_id`, `first_name`, `last_name`, `player_number`, `team_id`, `post_id`, `hands_id`, `date_of_birth`, `city_of_birth_id`, `country_of_birth_id`, `bio`, `avatar_photo`, `header_photo`, `player_profile_link`) VALUES
(1, 'Martin', 'Erat', 10, 1, 3, 1, '1981-08-29', 2, 1, '', 'erat.png', 'erat.png', 'https://www.eliteprospects.com/player/8611/martin-erat'),
(3, 'Leoš', 'Čermák', 12, 1, 3, 2, '1978-03-13', 2, 1, '', 'cermak.png', 'cermak.png', 'https://www.eliteprospects.com/player/15131/leos-cermak'),
(4, 'Vlastimil', 'Bubník', 12, 1, 3, 2, '1931-03-18', 4, 1, '', 'bubnik.png', 'bubnik.png', 'https://www.eliteprospects.com/player/66544/vlastimil-bubnik'),
(5, 'Maurice', 'Richard', 9, 2, 3, 1, '1921-08-04', 3, 3, '', 'richard.png', 'richard.png', 'https://www.eliteprospects.com/player/22519/maurice-richard'),
(6, 'Patrick', 'Roy', 33, 2, 1, 1, '1965-10-05', 6, 3, '', 'roy.png', 'roy.png', 'https://www.eliteprospects.com/player/21308/patrick-roy'),
(7, 'Carey', 'Price', 31, 2, 1, 1, '1987-08-16', 5, 3, '', 'price.png', 'price.png', 'https://www.eliteprospects.com/player/21308/patrick-roy');

-- --------------------------------------------------------

--
-- Struktura tabulky `posts`
--

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL,
  `post_name` varchar(16) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `posts`
--

INSERT INTO `posts` (`post_id`, `post_name`) VALUES
(1, 'brankář'),
(2, 'obránce'),
(3, 'útočník');

-- --------------------------------------------------------

--
-- Struktura tabulky `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL,
  `team_name` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `league_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `year_of_establishment` year(4) NOT NULL,
  `home_arena` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `team_logo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `header_photo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `bio` mediumtext COLLATE utf8_czech_ci NOT NULL,
  `number_of_championships` int(11) NOT NULL,
  `team_profile_link` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `teams`
--

INSERT INTO `teams` (`team_id`, `team_name`, `league_id`, `city_id`, `country_id`, `year_of_establishment`, `home_arena`, `team_logo`, `header_photo`, `bio`, `number_of_championships`, `team_profile_link`) VALUES
(1, 'Kometa Brno', 2, 1, 1, 1953, 'DRFG Aréna', 'kometa.png', 'kometa.png', '', 13, 'https://www.eliteprospects.com/team/1190/hc-kometa-brno'),
(2, 'Montreal Canadiens', 1, 3, 3, 1909, 'Bell Centre', 'canadiens.png', 'canadiens.png', '', 24, 'https://www.eliteprospects.com/team/64/montreal-canadiens');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`character_id`);

--
-- Klíče pro tabulku `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Klíče pro tabulku `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Klíče pro tabulku `hands`
--
ALTER TABLE `hands`
  ADD PRIMARY KEY (`hands_id`);

--
-- Klíče pro tabulku `leagues`
--
ALTER TABLE `leagues`
  ADD PRIMARY KEY (`league_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Klíče pro tabulku `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`player_id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `hands_id` (`hands_id`),
  ADD KEY `city_of_birth_id` (`city_of_birth_id`),
  ADD KEY `country_of_birth_id` (`country_of_birth_id`);

--
-- Klíče pro tabulku `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Klíče pro tabulku `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`),
  ADD KEY `league_id` (`league_id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `country_id` (`country_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `characters`
--
ALTER TABLE `characters`
  MODIFY `character_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pro tabulku `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pro tabulku `hands`
--
ALTER TABLE `hands`
  MODIFY `hands_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `leagues`
--
ALTER TABLE `leagues`
  MODIFY `league_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pro tabulku `players`
--
ALTER TABLE `players`
  MODIFY `player_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pro tabulku `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pro tabulku `teams`
--
ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `leagues`
--
ALTER TABLE `leagues`
  ADD CONSTRAINT `leagues_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`);

--
-- Omezení pro tabulku `players`
--
ALTER TABLE `players`
  ADD CONSTRAINT `players_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`),
  ADD CONSTRAINT `players_ibfk_2` FOREIGN KEY (`hands_id`) REFERENCES `hands` (`hands_id`),
  ADD CONSTRAINT `players_ibfk_3` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  ADD CONSTRAINT `players_ibfk_4` FOREIGN KEY (`city_of_birth_id`) REFERENCES `cities` (`city_id`),
  ADD CONSTRAINT `players_ibfk_5` FOREIGN KEY (`country_of_birth_id`) REFERENCES `countries` (`country_id`);

--
-- Omezení pro tabulku `teams`
--
ALTER TABLE `teams`
  ADD CONSTRAINT `teams_ibfk_1` FOREIGN KEY (`league_id`) REFERENCES `leagues` (`league_id`),
  ADD CONSTRAINT `teams_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `cities` (`city_id`),
  ADD CONSTRAINT `teams_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
