<?php
//nastavení připojení databáze
include_once('scripts/connection.php');
//obsah stránky
if(isset($_GET["page"])){
	$page = $_GET["page"];
} else {
	$page = "home";
}
//změna title a záhlaví
$title;
$heroText;
$header;	
switch ($page) {
	case 'home':
		$title = "Hlavní stránka";
		$heroText = "Lední<br>Hokej";
		$header = "iceHockey";
		break;

	case 'players':
		$title = "Hráči";
		$heroText = $title;
		$header = "players";
		break;

	case 'teams':
		$title = "Týmy";
		$heroText = $title;
		$header = "teams";
		break;

	case 'leagues':
		$title = "Ligy";
		$heroText = $title;
		$header = "leagues";
		break;

	case 'minigame':
		$title = "Minihra";
		$heroText = $title;
		$header = "iceHockey";
		//TODO:
		break;

	default:
		$title = "Hlavní stránka";
		$heroText = "Lední<br>Hokej";
		$header = "iceHockey";
		break;
}
?>
<!--Začátek HTML-->
<!DOCTYPE html>
<html lang="cs">
<head>
	<!--meta informace-->
	<meta name="author" content="Filip">
	<meta name="description" content="Stránky o ledním hokeji">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="theme-color" content="#12263F">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo $title?> | Filipův hokej</title>
	<!--import-->
	<link rel="manifest" href="manifest.json">
	<link rel="icon" href="ico/icon_512.png">
	<link rel="stylesheet" href="css/mobile.css" type="text/css" media="screen and (max-width:1023px)">
	<link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen and (min-width:1024px)">
</head>
<body>
	<!--Menu-->
	<nav>
		<a class="logo" href="index.php?page=home"><b>Filipův</b>Hokej</a>
		<ul id="menuLinks" style="media(screen and (max-width:1023px){display:none;}">
			<li><a href="index.php?page=home">Domů</a></li>
			<li><a href="index.php?page=players">Hráči</a></li>
			<li><a href="index.php?page=teams">Týmy</a></li>
			<li><a href="index.php?page=leagues">Ligy</a></li>
			<li><a href="index.php?page=minigame">Minihra</a></li>
		</ul>
		<a href="javascript:void(0)" id="menuBtn" onclick="menuToggle()">&#9776;</a>
	</nav>
	<!--Záhlaví-->
	<style>
		.heroImg{
			background-image:url(./img/headers/<?php echo $header;?>.png);
		}
	</style>
	<header>
		<h1 class="heroText"><?php echo $heroText;?></h1>
		<div class="heroImg"></div>
	</header>
	<!--Obsah-->
	<?php
		$title;
		switch ($page):
			case 'home':
				include "pages/home.php";
				$title = "Hlavní stránka";
				break;
			
			case 'players':
				include "pages/players.php";
				$title = "Hráči";
				break;

			case 'teams':
				include "pages/teams.php";
				$title = "Týmy";
				break;
			case 'leagues':
				include "pages/leagues.php";
				$title = "Ligy";
				break;
			
			case 'rules':
				include "pages/rules.php";
				break;

			case 'minigame':
				include "pages/minigame.php";
				break;

			default: 
				include "pages/home.php";
				break;
		endswitch;
	?>
	<!--Zápatí-->
	<script src="scripts/menu.js"></script>
	<?php require_once('scripts/footer.php')?>
</body>
</html>
